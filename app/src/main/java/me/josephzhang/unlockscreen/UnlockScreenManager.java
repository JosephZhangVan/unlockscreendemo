package me.josephzhang.unlockscreen;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * Created by jucyzhang on 15-07-25.
 */
public class UnlockScreenManager {

  private Context mContext;
  private ComponentName mDeviceAdmin;
  private DevicePolicyManager mManager;

  private UnlockScreenManager(Context context) {
    this.mContext = context;
    this.mDeviceAdmin = new ComponentName(context, DeviceAdminReceiver.class);
    this.mManager = (DevicePolicyManager) mContext
        .getSystemService(Context.DEVICE_POLICY_SERVICE);

  }

  public static UnlockScreenManager getInstance(Context context) {
    return new UnlockScreenManager(context);
  }

  public void lockScreen() {
    mManager.lockNow();
  }

  public void resetPassword(String newPassword) {
    mManager.resetPassword(newPassword, 0);
  }

  public boolean isDeviceAdminActive() {
    return mManager.isAdminActive(mDeviceAdmin);
  }

  public void enableDeviceAdmin(Activity activity) {
    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdmin);
    activity.startActivity(intent);
  }

}
