package me.josephzhang.unlockscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by jucyzhang on 15-07-25.
 */
public class TriggerActivity extends Activity implements View.OnClickListener,
    Runnable {

  private Handler mHander;
  private Button mBtnAction;
  private Button mBtnSetPassword;
  private UnlockScreenManager mManager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mManager = UnlockScreenManager.getInstance(this);
    setContentView(R.layout.main);
    mBtnSetPassword = (Button) findViewById(R.id.btn_set_password);
    mBtnAction = (Button) findViewById(R.id.btn_action);
    mHander = new Handler();
    mBtnAction.setOnClickListener(this);
    mBtnSetPassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
        startActivity(intent);
        Toast.makeText(getBaseContext(),
            "Please Setup Your Lock Screen Password", Toast.LENGTH_LONG).show();
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (!mManager.isDeviceAdminActive()) {
      mBtnAction.setText("Enable Device Admin");
    } else {
      mBtnAction.setText("Lock Screen and test");
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  public void onClick(View view) {
    if (!mManager.isDeviceAdminActive()) {
      mManager.enableDeviceAdmin(this);
    } else {
      mManager.lockScreen();
      mHander.postDelayed(this, 10000);
    }
  }

  @Override
  public void run() {
    mManager.resetPassword("");
    Intent intent = new Intent(this, DismissKeyguardActivity.class);
    startActivity(intent);
  }
}
