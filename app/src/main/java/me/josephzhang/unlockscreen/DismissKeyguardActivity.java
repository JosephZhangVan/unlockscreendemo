package me.josephzhang.unlockscreen;

import android.app.Activity;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.WindowManager;

/**
 * Created by jucyzhang on 15-07-25.
 */
public class DismissKeyguardActivity extends Activity {
  PowerManager.WakeLock mLock;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.unlock);
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    mLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(
        PowerManager.SCREEN_BRIGHT_WAKE_LOCK
            | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
    mLock.acquire();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    mLock.release();
  }
}
